function varargout = gui(varargin)
% GUI MATLAB code for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 03-Jan-2014 13:53:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;

% Set defaults
handles.eye = [0.5 0.5 0];
handles.light = [1 1 1];

set_eye_vec(hObject, eventdata, handles);

set(handles.light_x, 'String', num2str(handles.light(1),'%.2f'));
set(handles.light_y, 'String', num2str(handles.light(2),'%.2f'));
set(handles.light_z, 'String', num2str(handles.light(3),'%.2f'));

fp = get(handles.padding_slider, 'value');
set(handles.padding_text, 'String', num2str(fp,'%.2f'));
fs = get(handles.sampling_slider, 'value');
set(handles.sampling_text, 'String', num2str(fs,'%.2f'));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function sampling_slider_Callback(hObject, eventdata, handles)
% hObject    handle to sampling_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

fs = get(hObject, 'value');
set(handles.sampling_text, 'String', num2str(fs,'%.2f'));

% --- Executes during object creation, after setting all properties.
function sampling_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sampling_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function padding_slider_Callback(hObject, eventdata, handles)
% hObject    handle to padding_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
fp = get(hObject, 'value');
set(handles.padding_text, 'String', num2str(fp,'%.2f'));

% --- Executes during object creation, after setting all properties.
function padding_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to padding_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in interpolation_menu.
function interpolation_menu_Callback(hObject, eventdata, handles)
% hObject    handle to interpolation_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns interpolation_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from interpolation_menu

contents = cellstr(get(hObject,'String'));
handles.interpolation = contents{get(hObject,'Value')};
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function interpolation_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to interpolation_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'Value',2)
contents = cellstr(get(hObject,'String'));
handles.interpolation = contents{get(hObject,'Value')};
guidata(hObject, handles);

% --- Executes on selection change in shading_menu.
function shading_menu_Callback(hObject, eventdata, handles)
% hObject    handle to shading_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns shading_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from shading_menu
contents = cellstr(get(hObject,'String'));
value = contents{get(hObject,'Value')};

% Re-render image if a new field was selected.
if ~strcmp(handles.shading, value)
    handles.shading = value;
    guidata(hObject, handles);

    render_image(hObject, eventdata, handles);
end

% --- Executes during object creation, after setting all properties.
function shading_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to shading_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'Value',1)
contents = cellstr(get(hObject,'String'));
handles.shading = contents{get(hObject,'Value')};
guidata(hObject, handles);


% --- Executes on button press in render_button.
function render_button_Callback(hObject, eventdata, handles)
% hObject    handle to render_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

render_image(hObject, eventdata, handles);

% --------------------------------------------------------------------
function open_tool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to open_tool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile('*.vol','Select the .vol volumetric data file.');

if file == 0
   return 
end

if isfield(handles, 'image')
    handles = rmfield(handles, 'image');
    guidata(hObject, handles);
    cla(handles.center_axes,'reset')
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    set(gca,'ytick',[])
    set(gca,'yticklabel',[])
end

set(findobj(handles.figure1,'Enable','on'),'Enable','off');

set(handles.processing_text, 'String', 'Preprocessing ...');
set(handles.processing_text, 'visible', 'on');
set(handles.figure1, 'pointer', 'watch')
drawnow;

fp = get(handles.padding_slider, 'value');
fs = get(handles.sampling_slider, 'value');

% Parse the input file
f = parse_vol(strcat(path,file));

% Premultiply if activated
if get(handles.spatial_check, 'value')
    f = premultiply(f, handles.interpolation, fp, fs);
end

[F, ~, DF] = preprocess(f, fp);

handles.magnitude = DF;
handles.spectrum = F;

set(handles.processing_text, 'String', 'Calculating view ...');

render_image(hObject,eventdata,handles);

set(handles.processing_text, 'visible', 'off');
set(handles.figure1, 'pointer', 'arrow');
set(findobj(handles.figure1,'Enable','off'),'Enable','on');

% --------------------------------------------------------------------
function save_tool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to save_tool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'image')
    [file,path] = uiputfile({'*.tif;*.jpg;*.png;*.gif','Image Files';...
          '*.*','All Files' }, 'Save Rendering', 'Rendering');
      
    if file == 0
        return 
    end
    
    imwrite(mat2gray(handles.image), strcat(path,file));
end

% --- Executes on button press in spatial_check.
function spatial_check_Callback(hObject, eventdata, handles)
% hObject    handle to spatial_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of spatial_check


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'spectrum')
    handles.start_point = get(hObject, 'currentpoint');
    guidata(hObject, handles);
end

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonUpFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'start_point')
    handles = rmfield(handles, 'start_point');
    
    guidata(hObject, handles);
end

function render_image(hObject, eventdata, handles)
if isfield(handles, 'spectrum')
    set(findobj(handles.figure1,'Enable','on'),'Enable','off');
    
    set(handles.figure1, 'pointer', 'watch')
    drawnow;
    
    fs = get(handles.sampling_slider, 'value');
    fp = get(handles.padding_slider, 'value');
    
    img = grab_slice(handles.spectrum, handles.magnitude, handles.eye, ...
                     handles.light, handles.interpolation, handles.shading, fs, fp);

    set(handles.processing_text, 'visible', 'off');
    set(handles.figure1, 'pointer', 'arrow')

    set(gcf, 'currentaxes', handles.center_axes);

    imshow(img, []);
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    set(gca,'ytick',[])
    set(gca,'yticklabel',[])

    handles.image = img;
    
    set(handles.figure1, 'pointer', 'arrow')
    set(findobj(handles.figure1,'Enable','off'),'Enable','on');
    
    guidata(hObject, handles);
end


% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'start_point')
    curr = get(hObject, 'currentpoint');
    delta = curr - handles.start_point;
    
    if ~isempty(delta(abs(delta) > 4))
        delta = deg2rad(delta);
        
        % Rodriguez rotation (see http://mathworld.wolfram.com/RodriguesRotationFormula.html)
        rotate = @(a, w) eye(3) + [0 -w(3) w(2);w(3) 0 -w(1); -w(2) w(1) 0;]*sin(a)+[0 -w(3) w(2);w(3) 0 -w(1); -w(2) w(1) 0;]^2*(1-cos(a));

        % rotate about the volume center
        handles.eye = handles.eye - [0.5 0.5 0.5];
        handles.eye = (rotate(delta(1), [0 0 1])*rotate(delta(2), [0 1 0])*handles.eye')';
        handles.eye = handles.eye + [0.5 0.5 0.5];

        handles.start_point = curr;
        
        guidata(hObject, handles);
        
        set_eye_vec(hObject, eventdata, handles);
        
        % Retrigger render with new eye pos
        render_image(hObject,eventdata,handles);
    end
end



function eye_x_Callback(hObject, eventdata, handles)
% hObject    handle to eye_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eye_x as text
%        str2double(get(hObject,'String')) returns contents of eye_x as a double
val = get(hObject,'String');
val = str2double(val);

% Check value range and whether it is actually a number.
if isnan(val) || val > 1.00 || val < 0.00
    set(handles.eye_x, 'String', num2str(handles.eye(1),'%.2f'));
else
    % Update the parameter
    handles.eye(1) = val;
    set(handles.eye_x, 'String', num2str(handles.eye(1),'%.2f'));
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function eye_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eye_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function eye_y_Callback(hObject, eventdata, handles)
% hObject    handle to eye_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eye_y as text
%        str2double(get(hObject,'String')) returns contents of eye_y as a double
val = get(hObject,'String');
val = str2double(val);

% Check value range and whether it is actually a number.
if isnan(val) || val > 1.00 || val < 0.00
    set(handles.eye_y, 'String', num2str(handles.eye(2),'%.2f'));
else
    % Update the parameter
    handles.eye(2) = val;
    set(handles.eye_y, 'String', num2str(handles.eye(2),'%.2f'));
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function eye_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eye_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function eye_z_Callback(hObject, eventdata, handles)
% hObject    handle to eye_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eye_z as text
%        str2double(get(hObject,'String')) returns contents of eye_z as a double

val = get(hObject,'String');
val = str2double(val);

% Check value range and whether it is actually a number.
if isnan(val) || val > 1.00 || val < 0.00
    set(handles.eye_z, 'String', num2str(handles.eye(3),'%.2f'));
else
    % Update the parameter
    handles.eye(3) = val;
    set(handles.eye_z, 'String', num2str(handles.eye(3),'%.2f'));
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function eye_z_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eye_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function light_x_Callback(hObject, eventdata, handles)
% hObject    handle to light_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of light_x as text
%        str2double(get(hObject,'String')) returns contents of light_x as a double

val = get(hObject,'String');
val = str2double(val);

% Check value range and whether it is actually a number.
if isnan(val) || val > 1.00 || val < 0.00
    set(handles.light_x, 'String', num2str(handles.light(1),'%.2f'));
else
    % Update the parameter
    handles.light(1) = val;
    set(handles.light_x, 'String', num2str(handles.light(1),'%.2f'));
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function light_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to light_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function light_y_Callback(hObject, eventdata, handles)
% hObject    handle to light_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of light_y as text
%        str2double(get(hObject,'String')) returns contents of light_y as a double

val = get(hObject,'String');
val = str2double(val);

% Check value range and whether it is actually a number.
if isnan(val) || val > 1.00 || val < 0.00
    set(handles.light_y, 'String', num2str(handles.light(2),'%.2f'));
else
    % Update the parameter
    handles.light(2) = val;
    set(handles.light_y, 'String', num2str(handles.light(2),'%.2f'));
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function light_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to light_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function light_z_Callback(hObject, eventdata, handles)
% hObject    handle to light_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of light_z as text
%        str2double(get(hObject,'String')) returns contents of light_z as a double

val = get(hObject,'String');
val = str2double(val);

% Check value range and whether it is actually a number.
if isnan(val) || val > 1.00 || val < 0.00
    set(handles.light_z, 'String', num2str(handles.light(3),'%.2f'));
else
    % Update the parameter
    handles.light(3) = val;
    set(handles.light_z, 'String', num2str(handles.light(3),'%.2f'));
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function light_z_CreateFcn(hObject, eventdata, handles)
% hObject    handle to light_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function set_eye_vec(hObject, eventdata, handles)
set(handles.eye_x, 'String', num2str(handles.eye(1),'%.2f'));
set(handles.eye_y, 'String', num2str(handles.eye(2),'%.2f'));
set(handles.eye_z, 'String', num2str(handles.eye(3),'%.2f'));
