function [ F, s, DF ] = preprocess( data, fp )
%PREPROCESS Returns the frequency domain response of the 3D matrix
%__data__ and of its gradient magnitude. Optionally applies padding.
%
%   Author: Patrick Schwab <a0927193@unet.univie.ac.at>
%
%   Parameters:
%       data - The 3D volumetric data input.
%
%       fp - The padding rate factor. (1.0 = No padding)
%
%   Returns:
%       F - The frequency domain response (spectrum) of the 3D scalar input
%           data. F{data}
%
%       s - The size of the spectrum, after padding.
%
%       DF - The frequency domain response (spectrum) of the 3D scalar
%            input data's gradient magnitude. F{(data)'}

s = size(data);

% The padding on each side is half of the total padding.
s2 = ceil((fp-1)/2*s);

% Zero-Pad by factor fp
f = padarray(data, s2);

% Output the new size
s = s + (s2*2);

% Compute fft & do the shifts
F = fftshift(fftn(fftshift(f)));

% Compute the gradient in each dimension
[FX, FY, FZ] = gradient(f);

% Calculate the magnitude from the gradient vector
% We could probably do with the squared length here as a performance
% optimization, however the critical path is not the preprocessing anyway.
DF = sqrt(FX.^2 + FY.^2 + FZ.^2);

% And fft transform it
DF = fftshift(fftn(fftshift(DF)));

end

