function [ img ] = grab_slice( F, DF, eye, light, method, shading, fs, fp )
%GRAB_SLICE Extracts a slice of the fourier spectrum __F__ and transforms it
%to an image. __method__ is any of 'linear', 'cubic' and 'nearest'.
%__shading__ is one of 'None', 'Depth Cueing', 'Directional Shading' and
%'Combined'.
%
%   Author: Patrick Schwab <a0927193@unet.univie.ac.at>
%
%   Parameters:
%       F - The 3D volume spectrum F{f(x)}
%
%       DF - The 3D volume gradient spectrum F{df(x)}
%
%       eye - The eye position in normalized coordinates [0,1) x [0,1) x [0,1)
%
%       light - The light position in normalized coordinates [0,1) x [0,1) x [0,1)
%
%       method - The interpolation method to be used. One of 'linear', 'cubic' and 'nearest'.
%
%       shading - The shading algorithm to be used. One of 'None', 'Depth Cueing', 
%                 'Directional Shading' and 'Combined'.
%
%       fs - The sampling rate factor.
%
%       fp - The padding factor. (1.0 = No padding)
%
%   Returns:
%       img - The reconstructed projection in the spatial domain.
%
%   NOTE: Implemented on the basis of [1] 'Frequency Domain Volume Rendering',
%         by Totsuka, T. and Levoy, M., 1993

s = size(F);

% The center of the volume is our reference point
vol_center = [0.5 0.5 0.5];

% Compute the view and light vectors
view  = eye - vol_center;

% The light vector is in normalized coordinates [0,1) x [0,1) x [0,1)
% With [0.5, 0.5, 0.5] being the volume center.
% For further calculations we need to de-normalize it to actual 
% volume space coordinates.
light = (light - vol_center) .* s;

% Build an orthonormal basis
x = view;
nx = norm(x);

if nx ~= 0
    x = x / nx;
end

y = [0, -x(3), x(2)];
ny = norm(y);

if ny ~= 0
    y = y / ny;
end

z = cross(x, y);
nz = norm(z);

if nz ~= 0
    z = z / nz;
end
% -> x,y,z now define the orthonormal basis of our projection plane

% Define the (continuous) sample points
wu = linspace(-s(1)/2 + 1, s(1)/2, s(1)*fs);
wv = linspace(-s(2)/2 + 1, s(2)/2, s(2)*fs);

[wu, wv] = meshgrid(wu, wv);

idx = zeros(size(wu,1), size(wu,2), 3);

% Get our sampling plane's indices
idx(:,:,1) = y(1)*wu + z(1)*wv;
idx(:,:,2) = y(2)*wu + z(2)*wv;
idx(:,:,3) = y(3)*wu + z(3)*wv;

% Compute the center position of the volume
center = ceil(s/2);

x1 = center(1);
z1 = center(3);
xrange = -x1 + 1:x1;
% The input volume size must be the same in x and y dimension.
yrange = xrange;
zrange = -z1 + 1:z1;

% The z axis can have an arbitrary amount of samples. 
% They must, however, be linearly spaced apart.
zrange = linspace(zrange(1), zrange(end), s(3));

% Sample a slice by interpolation between the discrete volume values.
slice = interp3_proxy(xrange, yrange, zrange, F, ...
                      idx(:,:,1), idx(:,:,2), idx(:,:,3), method, 0);

% Apply shading according to __shading__
if strcmpi('Depth Cueing', shading)
    slice = apply_depth_cueing(slice, F, 1/2, x, ...
                               idx, xrange, yrange, zrange, method);
elseif strcmpi('Directional Shading', shading)    
    slice = apply_directional_shading(slice, DF, 1.97, 0.03, light, ...
                                      idx, xrange, yrange, zrange, method);
elseif strcmpi('Combined', shading)
    % As explained in [1] we have to apply the shading first. However, 
    % since we perform the two convolutions in the correct order,
    % we don't have to add a correction term.
    % Note that this choice was made, because the performance of a custom matlab
    % interpolation routine is about 1000 times slower than the intrinsic
    % interpolation routine, and thus the performance gain of combining the
    % convolutions in a single kernel are (more than) consumed by the performance loss
    % of employing such a custom interpolator.
    slice = apply_directional_shading(slice, DF, 1.97, 0.03, light, ...
                                      idx, xrange, yrange, zrange, method);
    slice = apply_depth_cueing(slice, F, 1/2, x, ...
                               idx, xrange, yrange, zrange, method);
end

% The reconstruction of our slice is the spatial domain projection of the given 3d volume.
slice = ifftshift(slice);
slice = ifftn(slice);

as = size(slice);

% Only the center part is relevant for our reconstruction.
os = as ./ (fs * fp);
os2 = floor((as - os)./2);

img = real(abs(fftshift(slice)));
img = img(os2(1):end-os2(1), os2(2):end-os2(2));

    function slice = interp3_proxy(xr,yr,zr,V,idx,idy,idz,method,nanval)
        if strcmpi('Windowed Sinc', method)
            T = 1/fs;
            len = 3;

            % Using a hamming window
            ham = @(n) 0.54 + 0.46*cos(2.*pi.*(1/T).*n ./ (len-1));

            % And a sinc filter for interpolation.
            filter = @(x) (1/T).*sinc(x).*ham(x);
            
            % This call performs 3d interpolation in matlab (which is about
            % 1000 times slower than the intrinsic 3d interpolation) -
            % If performance is of concern the intrinsic routine should be
            % used.
            slice = cinterp(xr,yr,zr,V,idx,idy,idz,len,filter);
        else
            slice = interp3(xr, yr, zr, V, ...
                            idx, idy, idz, method, nanval);
        end
    end

    function slice = apply_depth_cueing(slice, F, c_cue, x, idx, xrange, yrange, zrange, method)
        % As suggested in [1] - we perform an offset evaluation using an
        % approximately linear depth cueing model.

        % The Fourier Transform of a sine wave is two impulses of different
        % offsets. We only use one for our output image, for performance reasons.
        % The constant and offset are defined by the amplitude and 
        % period of the sine wave we use to approximate linear depth cueing.
        c_imp = -1/c_cue*sqrt(pi/2)*1i;
        c = sqrt(pi/2);
        offset = 1;

        % The depth cued slice is offset by a distance __offset__, 
        % as calculated previously, in the view direction __x__.
        offset = offset .* x;

        % Offset the indices to get the perception of depth cueing.
        idx(:,:,1) = idx(:,:,1) + offset(1);
        idx(:,:,2) = idx(:,:,2) + offset(2);
        idx(:,:,3) = idx(:,:,3) + offset(3);

        % Sample an offset slice and sum it with the constant term as explained in [1].
        slice = c     .* slice + ...
                c_imp .* interp3_proxy(xrange, yrange, zrange, F, ...
                                       idx(:,:,1), idx(:,:,2), idx(:,:,3), method, 0);
    end

    function slice = apply_directional_shading(slice, DF, c_amb, c_dif, light, idx, xrange, yrange, zrange, method)
        % We could set light colors here - tho, keeping it grayscale is faster.
        l_amb = 1;%[1 1 1];
        l_dif = 1;%[1 1 1];

        s1 = idx(:,:,1);
        s2 = idx(:,:,2);
        s3 = idx(:,:,3);

        % Compute the light vector
        l1 = light(1) - s1;
        l2 = light(2) - s2;
        l3 = light(3) - s3;

        % Normalize the light vector (NaN guarded)
        nl = sqrt(l1.^2 + l2.^2 + l3.^2);
        l1(nl ~= 0) = l1(nl ~= 0) ./ nl(nl ~= 0);
        l2(nl ~= 0) = l2(nl ~= 0) ./ nl(nl ~= 0);
        l3(nl ~= 0) = l3(nl ~= 0) ./ nl(nl ~= 0);

        % Compute the dot product with the light vector for each matrix entry.
        sdotl = (s1.*l1 + s2.*l2 + s3.*l3);

        % Sample a slice of the gradient magnitude volume.
        dslice = interp3_proxy(xrange, yrange, zrange, DF, ...
                               idx(:,:,1), idx(:,:,2), idx(:,:,3), method, 0);

        % Insert it all into the lighting equation from [1]
        slice = (c_amb.*l_amb + 1i*pi.*c_dif.*l_dif.*sdotl) .* slice + ...
                (1/2).*c_dif.*l_dif .* dslice;
    end
end

