function [ data ] = premultiply( data, method, fp, fs )
%PREMULTIPLY Premultiplies __data__ by performing the inverse transform on
%the used interpolation method and multiplying with the first cycle of the
%resulting spatial response.
%
%   Author: Patrick Schwab <a0927193@unet.univie.ac.at>
%
%   Parameters:
%       data - The premultiplied 3D volumetric data input.
%
%       method - The interpolation kernel for which to apply the
%                correction. One of 'nearest', 'linear' and 'cubic'.
%
%       fp - The padding rate factor.
%
%       fs - The sampling rate factor.
%
%   Returns:
%       data - The premultiplied 3D volumetric data output.

% The frequency responses of all our available interpolation methods are based
% on the sinc function. (Spatial response would be more fitting in this
% context..)
if strcmpi(method, 'nearest') 
    fr = @(x) sinc(x);
elseif strcmpi(method, 'linear')
    fr = @(x) sinc(x).^2;
elseif strcmpi(method, 'cubic')
    fr = @(x) sinc(x).^4;
else
    error('Method must be one of "nearest", "linear" and "cubic"');
end

s = size(data);

% We're interested in the first cycle [-pi, pi) only
r = [-0.5 0.5] ./ (fp * fs);

xr = linspace(r(1),r(2),s(1));
yr = linspace(r(1),r(2),s(2));
zr = linspace(r(1),r(2),s(3));

[x,y,z] = ndgrid(xr,yr,zr);

% The reciprocal of the frequency/spatial response of our interpolation method
% is our multiplication factor.
% The filter is seperable thus we can simply evaluate each axis on its own.
data = data .* (1./(fr(x) .* fr(y) .* fr(z)));

end

