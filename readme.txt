
___________                 .__               ____   ____    .__                         __________                   .___                          
\_   _____/___  __ _________|__| ___________  \   \ /   /___ |  |  __ __  _____   ____   \______   \ ____   ____    __| _/___________   ___________ 
 |    __)/  _ \|  |  \_  __ \  |/ __ \_  __ \  \   Y   /  _ \|  | |  |  \/     \_/ __ \   |       _// __ \ /    \  / __ |/ __ \_  __ \_/ __ \_  __ \
 |     \(  <_> )  |  /|  | \/  \  ___/|  | \/   \     (  <_> )  |_|  |  /  Y Y  \  ___/   |    |   \  ___/|   |  \/ /_/ \  ___/|  | \/\  ___/|  | \/
 \___  / \____/|____/ |__|  |__|\___  >__|       \___/ \____/|____/____/|__|_|  /\___  >  |____|_  /\___  >___|  /\____ |\___  >__|    \___  >__|   
     \/                             \/                                        \/     \/          \/     \/     \/      \/    \/            \/     

 Author: Patrick Schwab, 03.01.2014

 Description
--------------------------------------------

The Fourier Volume Renderer application renders 3D scalar data, input as .vol files, in the frequency domain, based on the algorithms described in FREQUENCY DOMAIN VOLUME RENDERING, by Totsuka, T and Levoy, M., and
FOURIER VOLUME RENDERING, by Malzbender, T.

The volumetric data is firstly preprocessed and transformed in the frequency domain. After the preprocessing step, the Fourier Volume Renderer generates orthographic projections, from different viewpoints, by extracting a slice from the 3D frequency spectrum of the volumetric data.

The renderer is fully parametrizable and supports spatial premultiplication, multiple interpolation kernels and multiple shading models, including Depth Cueing, Directional Shading and Combined Depth Cueing with Directional Shading.

While it is a matlab application the performance-wise optimal settings allow for fast slice extraction within as fast as a quarter of a second on average hardware.

Interesting renderings are provided in the 'interesting/' directory within the distributed source code bundle.

 Installation
--------------------------------------------

The program is distributed as 1- a Matlab Application Package (Fourier Volume Renderer.mlappinstall) and 2- in source code form.

1-
NOTE: Matlab APPS require at least Matlab version R2008.
To install as a Matlab Application, simply double click the .mlappinstall package, which will then proceed to install the program to your Matlab IDE.
It can then be called from the APPS - Tab, from within your Matlab IDE. 

2-
In case your Matlab installation does not support Matlab APPS the program scripts can be invoked from the commandline:

	Change to the directory containing the matlab source files:
	 cd 'path/to/fourier_volume_renderer'

	Start the program's GUI:
	 gui


 Usage
--------------------------------------------

To open a .vol file, click the "open" icon in the application's toolbar and choose the file you wish to render. The program will automatically preprocess the data according to your settings and render an initial projection to the left hand side of the GUI.

To save a rendered projection, click the "save" icon in the application's toolbar and choose the output file name and extension.

The following aspects settings can be adjusted, on the right hand side of the GUI:

Sampling Rate: 
The rate at which the image is interpolated during the slice extraction process. Can be any value within [1.00, 2.00].
A higher rate leads to less aliasing (especially at specific viewing angles), but also incurrs a significant performance overhead.
The default value (1.5) is a good trade-off between performance and rendering quality.

Padding Rate:
The amount of zero-padding to be applied during the preprocessing of the volumetric dataset. A higher rate increases the rendering
time significantly, but helps prevent aliasing artifacts. Important: This setting has to be changed PRIOR to loading the .vol file, as
the padding is applied in the preprocessing step. Changing this slider will NOT have any effect until the next time a file is opened.
Can be any value within [1.00, 2.00].

Interpolation Method:
The interpolation kernel to use when extracting a slice from the frequency spectrum. As with the other options this setting
allows for a trade-off between rendering performance and image quality. Note that a windowed sinc interpolation kernel is not
available because of the (enormous) performance overhead associated with using a custom interpolation function in matlab.
However, the file showcasing such a custom windowed-sinc interpolation is added in the application's source bundle for reference / documentary purposes.

Shading: 
The shading algorithm to be applied on the reconstructed projection.

Eye Position: 
The viewer's eye position defining the projection angle. In normalized coordinates [0,1) x [0,1) x [0,1). Always pointing towards the
center of the volume (at [0.5, 0.5, 0.5]). Alternatively, the eye position can also be adjusted by clicking and dragging the 
projection image.

Light Position: 
The position of the light source. Only relevant for the 'Directional Shading' mode. In normalized coordinates [0,1) x [0,1) x [0,1).

Spatial Premultiply: 
Whether or not the volumetric data should have a correction function applied before transformation to the frequency domain. The
correction function adjusts the input data, so that our interpolation kernels, which are not 1 in the full first cycle, are 
accounted for in advance.

