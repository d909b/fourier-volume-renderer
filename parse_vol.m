function [ vol, size ] = parse_vol( filename )
%PARSE_VOL Parses the volumetric data .vol file at __filename__ into a MxNxK Matrix.
%
%   Author: Patrick Schwab <a0927193@unet.univie.ac.at>
%
%   Parameters:
%       filename - The filepath to the 3D volumetric data.
%
%   Returns:
%       vol - The MxNxK Matrix corresponding to the volumetric data at
%             __filename__.
%
%       size - The size of the volumetric data.

fp = fopen(filename, 'r');

% Cleanup at exit.
cleanup = onCleanup(@()fclose(fp));

c = 0;
s = [];
size = zeros(1,3);
i = 1;
while true
    c = char(fread(fp, 1, '*uint8'));
    
    if c == 32 || c == 10
        size(i) = str2double(cellstr(s));
        i = i+1;
        s = [];

        if c == 10
            break;
        end
    else
        s = [s c];
    end
end

vol = zeros(size);

for i=1:size(3)
    vol(:,:,i) = fread(fp, [size(1) size(2)], '*uint8');
end

end

