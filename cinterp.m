function [ outvals ] = cinterp( xrange, yrange, zrange, vals, xidx, yidx, zidx, len, kernel )
%CINTERP Custom 3D interpolation routine, employing an arbitrary
%interpolation __kernel__.
%
%   Author: Patrick Schwab <a0927193@unet.univie.ac.at>
%
%   Parameters:
%       xrange/yrange/zrange - The indices corresponding to the entries in
%                              __vals__.
%
%       vals - The 3D data to be interpolated.
%
%       xidx/yidx/zidx - The continuous indices to be evaluated by the
%                        interpolation kernel.
%
%       len - The length of the interpolation kernel
%
%       kernel - The interpolation kernel to use (1-dimensional, separable).
%
%   Returns:
%       outvals - The reconstructed values at xidx/yidx/zidx.
%
%   NOTE: Not actually used in the application, because its (more than) 
%         an order of magnitude slower than the intrinsic interpolation 
%         routine interp3. It remains in the package for
%         documentation reasons only.

[sx, sy] = size(xidx);
hs = floor(len ./ 2);

center = [-xrange(1), -yrange(1), -zrange(1)];

x = xidx + center(1);
y = yidx + center(2);
z = zidx + center(3);

s = size(vals);

x = x + hs;
y = y + hs;
z = z + hs;

outvals = zeros(sx,sy);
for i=1+hs:sx-hs
    for j=1+hs:sy-hs
        xc = round(x(i,j));
        yc = round(y(i,j));
        zc = round(z(i,j));
       
        % Store the rounded (closest points) indices (discrete)
        xv = xc-hs:xc+hs;
        yv = yc-hs:yc+hs;
        zv = zc-hs:zc+hs;
        
        dx = xc - x(i,j);
        dy = yc - y(i,j);
        dz = zc - z(i,j);
        
        % And the actual points (continuous)
        dx = dx-hs:dx+hs;
        dy = dy-hs:dy+hs;
        dz = dz-hs:dz+hs;
        
        % Sanity checks
        xv(xv < 0) = 0;
        yv(yv < 0) = 0;
        zv(zv < 0) = 0;
        xv(xv >= s(1)) = 0;
        yv(yv >= s(2)) = 0;
        zv(zv >= s(3)) = 0;
        
        % Get the cube corresponding to the closest nxnxn points in the
        % grid.
        A = vals(xv + 1, yv + 1, zv + 1);
        
        % Perform interpolation using the passed kernel function.
        % The kernel is seperable and as such it can be used on each
        % dimension and later combined.
        dx = kernel(dx);
        dy = kernel(dy);
        dz = kernel(dz);
        
        % Build a cube of weights
        mx = repmat(dx', [1 len len]);
        my = repmat(dy, [len 1 len]);
        mz = ones(len,len,len);
        
        for t=1:len
           mz(:,:,t) = mz(:,:,t) * dz(t);
        end
        
        outvals(i,j) = sum(sum(sum(A.*mx.*my.*mz)));
    end
end
end

